#!/bin/bash

Path=/home/$(whoami)
logPath=/home/$(whoami)/log
Date=$(date "+%Y%m%d%H%M%S")

mkdir -p $logPath

printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n" > $logPath/metrics_$Date.log

free -m | awk '
/Mem/{printf"%d,%d,%d,%d,%d,%d,",$2,$3,$4,$5,$6,$7}
/Swap/{printf"%d,%d,%d,",$2,$3,$4}' >> $logPath/metrics_$Date.log

A=`du -sh $Path` 
read -a A <<< $A
printf "%s,%s" ${A[1]} ${A[0]} >> $logPath/metrics_$Date.log

chmod 400 $logPath/metrics_$Date.log
