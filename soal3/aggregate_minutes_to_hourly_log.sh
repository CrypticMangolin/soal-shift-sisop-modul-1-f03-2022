#!/bin/bash

Path=/home/$(whoami)
logPath=/home/$(whoami)/log
Date=$(date -d "1 hour ago" "+%Y%m%d%H")

awk -F "," 'BEGIN{}
FNR==1{next}
NR==2{
    for (i=1; i<=11; i++) {
        sum[i]=$i
        min[i]=$i
        max[i]=$i
    }
    count++
}
{
    for (i=1; i<=11; i++) {
        if (i==10) continue # skip path
        sum[i]+=$i
        if (min[i]>$i) min[i]=$i
        if (max[i]<$i) max[i]=$i
    }
    count++
}
END { 
    printf("type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n")
    
    printf("minimum")
    for (i=1; i<=11; i++) {
        if (i>0) printf(",")
        printf("%s",min[i])
    }
    printf("\n")

    printf("maximum")
    for (i=1; i<=11; i++) {
        if (i>0) printf(",")
        printf("%s", max[i])
    }
    printf("\n")
    
    printf("average")
    for (i=1; i<=11; i++) {
        if (i>0) printf(",")
        avg=sum[i]/count
        printf("%s", (i==10)?sum[i]:avg) # print path as is
        if (i==11) print substr(min[i],length(min[i]),1)
    }
}' $logPath/metrics_$Date*.log > $logPath/metrics_agg_$Date.log

chmod 400 $logPath/metrics_agg_$Date.log
