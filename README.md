# Soal Shift Sisop Modul 1 F03-2022

## Anggota Kelompok

| Nama                       | NRP        |
| :------------------------- | :--------- |
| Hidayatullah               | 5025201031 |
| Muhammad Daffa Aldriantama | 5025201177 |
| Atha Dzaky Hidayanto       | 5025201269 |

---
## Soal 1
### Penjelasan
Soal nomor 1 memerintahkan kita untuk membuat sistem register (register.sh) dan login (main.sh) akun. Akun tersebut akan digunakan untuk melakukan 2 command, yaitu `dl N` untuk mengunduh N gambar dan `att` untuk menampilkan banyaknya percobaan login. 

### Register
```bash
done=0
echo "Pendaftaran akun
====================="

until [ $done == 1 ]
do
    read -p "Username: " username;

    if grep -q "$username " ./users/user.txt
    then
        echo "$username sudah ada"
		echo
        log_write "1"
    else 
        done=1
    fi
done
```
Potongan kode di atas terdapat di dalam file `register.sh`. Potongan tersebut digunakan untuk memasukkan username yang ingin didaftarkan. Jika username sudah digunakan, program akan memperingatkan user.

Tampilan saat program dijalankan:

![alt text](https://imgur.com/3dK15ZM.png)

Tampilan jika username telah digunakan:

![alt text](https://imgur.com/u8uCqfI.png) 

```bash
password="kosong"
repeat="null"

until [ "$password" == "$repeat" ]
do
    done=0

    until [ $done == 1 ]
    do
        read -s -p "Password: " password
        echo
        if [ ${#password} -lt 8 ]
        then
            pass_requirements
        elif [[ ! $(echo "$password" | awk '/[a-z]/ && /[A-Z]/ && /[0-9]/') ]]
        then
            pass_requirements
        elif [[ "$password" == "$username" ]]
        then
            printf "Password tidak boleh sama dengan username!\n\n"
        else
            done=1
        fi
    done

    read -s -p "Repeat your password:" repeat
    if [ $password != $repeat ]
        then printf "\nPassword tidak sama. Masukkan kembali password anda.\n\n"
    fi
    
done
```
Potongan Kode di atas digunakan untuk membuat password. Ketika diinput, password tidak ditampilkan. Password diperiksa apakah telah memenuhi syarat yang ditulis dalam soal *1 poin b*, yaitu:
1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
3. Alphanumeric
4. Tidak boleh sama dengan username

Tampilan ketika password tidak memenuhi syarat:

![Sama dengan username](https://imgur.com/PBL2FXx.png)

![Tidak sesuai syarat](https://imgur.com/AhLo8zV.png)

Dalam potongan kode tersebut, user akan diminta untuk memasukkan passwordnya kembali. Jika tidak sama, user perlu memasukkan kembali passwordnya. Jika sudah sama, pendaftaran akan berhasil dan akun dapat digunakan.

Tampilan ketika password tidak sama:

![Repeat password tidak sama](https://imgur.com/cPbxbQ9.png)

Tampilan ketika pendaftaran berhasil:

![Pendaftaran berhasil](https://imgur.com/3CjaUby.png)

Tampilan di dalam user.txt:

![Users.txt](https://imgur.com/I7k7d5l.png)

### Main
```bash
isLoggedIn=0

until [ $isLoggedIn == 1 ]
do
    read -p "Username: " username;
    read -s -p "Password: " password;


    if grep -q "$username " ./users/user.txt
    then
        if [ $password == $(awk -v id="$username " '$0 ~ id {print $2}' users/user.txt) ]
        then
            echo
            echo "Login berhasil!"
            isLoggedIn=1
            log_write "4" "$username" 
        else
            printf "\nPassword salah!\n\n"
            log_write "3" "$username" 
        fi
    else
        printf "\nUsername tidak ditemukan!\n\n"
    fi
done
```
Potongan kode di atas dipakai untuk melakukan login. User akan diminta memasukkan username dan password yang telah didaftarkan.

```bash
echo "Command bisa dimulai"

isExit=0

until [ $isExit == 1 ]
do
    printf "\nCommand:
dl N - Mengunduh N gambar
att - Menghitung jumlah percobaan login
exit - Keluar dari program\n"

    read cmd N

    case "$cmd" in
    "dl")
        dl_image "$N" "$username"
        echo "$N gambar berhasil diunduh"
        ;;
    "att")
        echo -n "Jumlah percobaan login: "
        log_count "$username"
        ;;
    "exit")
        isExit=1
        echo "Logging out.."
        ;;
    esac
done
```
Jika berhasil, akan dilanjutkan dengan potongan kode ini. Potongan tersebut akan menampilkan command apa saja yang dapat dijalankan oleh user. Berikut tampilan user yang telah berhasil login:

![Berhasil Login](https://imgur.com/96BpeaZ.png)

#### dl N

Ketika menjalankan command `dl N`, fungsi yang dijalankan adalah `dl_image()`
```bash
dl_image() {
    zipPass=$(awk -v id="$2 " '$0 ~ id {print $2}' users/user.txt)

    currTime=`date +"%Y-%m-%d"`
    fileDir="${currTime}_$2"

    startNum=0

    if [ -f "$fileDir.zip" ]
    then 
        unzip -P $zipPass $fileDir.zip
        rm $fileDir.zip

        cd $fileDir
        startNum=`ls PIC_* | sort -n | tail -1 | tr -dc '0-9'`
        cd ..
    else
        mkdir -p $fileDir
    fi

    for i in $( eval echo {1..$1} )
    do
        x=$(($startNum+$i))
        wget https://loremflickr.com/320/240 -O $fileDir/PIC_$(echo $x | awk '{ printf "%02d", $1}').jpeg
    done

    if [ -f "$fileDir.zip" ]
    then rm $fileDir.zip
    fi

    zip -P $zipPass -r $fileDir.zip $fileDir
    rm -r $fileDir
}
```
Alur dari fungsi tersebut adalah sebagai berikut:
1. Fungsi akan mengambil password zip dari `users.txt` yang kemudian akan disimpan di dalam `zipPass`
2. Membuat variabel `FileDir` yang berisi nama file
3. Memeriksa apakah sudah ada zip dengan nama yang sama dengan nilai `FileDir`. Jika ada, zip tersebut akan di extract ke dalam folder/directory dengan nilai yang sama. Jika tidak, akan dibuat directory baru
4. Melakukan download sebanyak `N` gambar
5. Meng-compress folder di mana gambar berada  dan menghapusnya

Setelah itu, zip yang berisi gambar dapat terlihat

![Zip gambar](https://imgur.com/zwi2oCN.png)

![Isi dari zip](https://imgur.com/Fg1f4Qh.png)\

#### att
Ketika user menjalankan command `att`, fungsi yang dijalankan adalah `log_count()`
```bash
log_count() {
    awk -v name="$1" '($3=="LOGIN:ERROR" && $9==name) || ($3=="LOGIN:INFO" && $5==name) { count++ } END {print count}' log.txt
}
```
Fungsi tersebut akan menghitung total jumlah `LOGGIN:ERROR` dan `LOGGIN:INFO` dari suatu user di dalam `log.txt`

![Fungsi att](https://imgur.com/1fh7TWl.png)



### Log
Log adalah daftar record dari aktivitas register dan login pengguna. Dalam kedua program (`register.sh` dan `main.sh`), log ditulis dengan menggunakan fungsi `log_write()` dan akan disimpan ke dalam `log.txt`.
```bash
log_write() {
    currTime=`date +"%D %T"`
    logAct=$1

    case "$logAct" in
    "1")
        echo "$currTime REGISTER:ERROR User Already exists" >> log.txt
        ;;
    "2")
        echo "$currTime REGISTER:INFO User $2 registered successfully" >> log.txt
        ;;
    "3")
        echo "$currTime LOGIN:ERROR Failed login attempt on user $2 " >> log.txt
        ;;
    "4")
        echo "$currTime LOGIN:INFO User $2 logged in" >> log.txt
        ;;
    *)
        echo "ERROR LOG ACTION"
        ;;
    esac
}
```
Tampilan dari `log.txt` ketika dibuka:

![Tampilan log.txt](https://imgur.com/EIjufZF.png)

## Soal 2
### Cara Pengerjaan
**soal2_forensic_dapos.sh** <br/>
Script untuk memproses data dari file log `log_website_daffainfo.log`.

Hasil proses akan disimpan kedalam folder `forensic_log_website_daffainfo_log` sehingga perlu dibuat directorinya jika belum ada dengan mkdir
```bash
mkdir -p forensic_log_website_daffainfo_log
```

Untuk pemrosesan data menggunakan awk yang akan membaca file log dan menyimpannya ke file `hasil.txt`. File log menggunakan delimiter `:` sehingga menggunakan opsi `awk -F ":"`. Setelah itu, men-skip header pada baris pertama dengan `NR==1`.
```bash
awk -F ":" '
NR == 1 {next}
    # kode
' log_website_daffainfo.log > hasil.txt
```

**1.Mencari rata-rata request per jam**
```bash
awk -F ":" '
NR == 1 {next}
NR == 2 {StartHour=$3} 
    {ReqCount++}
END {
	TotalHour=$3-StartHour+1
	printf("Rata-rata serangan adalah sebanyak %f requests per jam", ReqCount/TotalHour) > "ratarata.txt"
}' log_website_daffainfo.log
```
Jam sebuah request dilakukan dapat diakses pada kolom ke-tiga. Karena salah satu field dalam log juga menggunakan delimiter `:`.

`NR == 2` adalah mengakses request pertama dalam log. `StartHour` akan menyimpan waktu (jam) terjadinya request pertama.

`{ReqCount++}` akan menghitung jumlah request yang dibaca.

Penghitungan waktu (jam) dilakukan setelah selesai membaca log. pada bagian `END` awk masih menyimpan baris yang terakhir dibaca, sehingga kolom 3 bisa langsung diakses untuk mendapatkan jam dari request terakhir. Setelah itu diselisihkan dengan `StartHour` untuk mendapat `TotalHour`.

Rata-rata dihitung dengan membagi `ReqCount` dengan `TotalHour` dan di print pada "ratarata.txt"

ratarata.txt: <br/>
![Avg](https://imgur.com/Bolm1eb.png)

**2.Mencari Ip dengan request terbanyak**
```bash
awk -F ":" '
NR == 1 {next}
{ipFreq[$1]++}
END {
    maxFreq=0; maxIp="0"
	for (ip in ipFreq) {
		if(maxFreq<ipFreq[ip]) {
			maxIp=ip
			maxFreq=ipFreq[ip]
		}
	}
	printf("IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n\n", maxIp, maxFreq)
}' log_website_daffainfo.log > hasil.txt
```
Untuk mendata Ip unik, disini menggunakan associative array `ipFreq` dengan Ip sebagai indexnya. Selain itu array juga berfungsi untuk mencatat banyaknya request dari suatu Ip. Hal tersebut dilakukan dalam `{ipFreq[$1]++}`.

Pencarian Ip dengan request terbanyak dilakukan dengan cara looping. Loop dilakukan untuk setiap ip dalam `ipFreq` untuk mencari nilai `maxFreq` serta mencatat `maxIp`. Setelah itu, `maxFreq` dan `maxIp` akan di print ke hasil.txt

**3.Menghitung jumlah penggunaan user agent curl**
```bash
awk -F ":" '
NR == 1 {next}
/curl/  {++curlCount}
END     {printf("Ada %d requests yang menggunakan curl sebagai user agent\n\n", curlCount)}    
' log_website_daffainfo.log > hasil.txt
```
`/curl/` akan mencari keyword "curl" dalam tiap baris. Dan jika ada, increment value dari `curlCount`

Setelah membaca semua baris, print `curlCount` ke hasil.txt

**4.Mendata Ip yang melakukan request pada jam 2**
```bash
awk -F ":" '
NR == 1 {next}
$3 == "02" {ipListJam2[$1]++}
END {
    for (ip in ipListJam2) {
        print ip
    }
}' log_website_daffainfo.log > hasil.txt
```
Cara mendata Ip sama dengan poin nomor 2, menggunakan associative array `ipListJam2` dengan Ip sebagai indexnya. Namun, baris yang digunakan hanya yang request pada jam "02". Hal itu dilakukan dengan memberi seleksi `$3 == "02"`.

Setiap ip pada array `ipListJam2` akan di print menggunakan loop.

**Implementasi** <br/>
Dalam implementasinya, 4 poin tersebut dilakukan dalam satu awk. Meskipun poin 1 di outputkan pada file yang berbeda, hasil print bisa langsung di-redirect ke file "ratarata.txt"

`hasil.txt` dan `ratarata.txt` akan dibuat di directory yang sama dengan directory script. Kedua file tersebut dipindahkan dengan menggunakan `mv`
```bash
mv hasil.txt ratarata.txt forensic_log_website_daffainfo_log
```
Hasil Run: <br/>
![Folder](https://imgur.com/3esDtv8.png)<br/>
Hasil.txt:<br/>
![Hasil](https://imgur.com/o1yF1gU.png)

---
## Soal 3
### Cara Pengerjaan
**minute_log.sh** <br/>
Script untuk mendata penggunaan ram dan disk pada directory `/home/{user}/` secara periodik tiap menit. 

Untuk menjalankan file ini tiap menit dapat menambahkan syntax berikut dalam `crontab -e`
``` 
* * * * * bash minute_log.sh
```

Data akan dituliskan pada file log dengan format nama `metrics_{YmdHMS}.log` dan disimpan pada directory `/home/{user}/log`
```bash
Path=/home/$(whoami)
logPath=/home/$(whoami)/log
Date=$(date "+%Y%m%d%H%M%S")

mkdir -p $logPath
```
Command `whoami` digunakan untuk mengetahui nama user saat ini. `Path` dan `logPath` disimpan dalam variable untuk memudahkan pengerjaan. Command `date` untuk mengetahui waktu saat ini dalam format (`YmdHMS`) dan digunakan untuk penamaan file log. Sebelum memulai, buat folder (beserta parent directorynya) jika belum ada menggunakan `mkdir -p`.

Print header ke dalam folder log dengan format nama `metrics_{YmdHMS}.log`.
```bash
printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n" > $logPath/metrics_$Date.log
```
Command `free -m` untuk melihat penggunaan memori saat ini. Hasilnya diproses dengan awk. Untuk baris tipe `Mem` akan ada 6 value, sedangkan untuk `Swap` hanya ada 3 value. Print setiap value dipisahkan dengan `","`
```bash
free -m | awk '
/Mem/{printf"%d,%d,%d,%d,%d,%d,",$2,$3,$4,$5,$6,$7}
/Swap/{printf"%d,%d,%d,",$2,$3,$4}' >> $logPath/metrics_$Date.log
```
Karena file output sudah dibuatkan sebelumnya, hasilnya akan di-*append* menggunakan simbol `>>`. 

Mengecek penggunaan disk dari suatu directory dengan command `du -sh $Path`.
```bash
A=`du -sh $Path` 
read -a A <<< $A
printf "%s,%s" ${A[1]} ${A[0]} >> $logPath/metrics_$Date.log
```
Karena format yg di inginkan dalam soal `... ,Path,Path_Size` sedangkan output `du -sh` sebaliknya. Maka hasil output `du -sh` akan disimpan dalam variable sebagai string. Lalu, menggunakan `read -a` untuk memecah string ke dalam array dengan whitespace sebagai pemisah defaultnya. Setelah itu, print `Path` terlebih dahulu dan di-*append* ke file yg sudah ada.

- Agar tidak ada newline setelah tiap command print, salah satu caranya adalah menggunakan printf.
```bash
chmod 400 $logPath/metrics_$Date.log
```
Ubah permission file menggunakan `chmod`. "4" menandakan read only untuk user saat ini. Sedangkan "0" tidak memiliki permission apapun pada file ini, yg ditujukan untuk group dan orang lainnya.
Hasil Run:<br/>
![Metrics](https://imgur.com/6R6Ce86.png)

**aggregate_minutes_to_hourly_log.sh** <br/>
Script untuk mengkalkulasi maximum, minimum, dan average penggunaan ram dan disk dari log hasil `minute_log.sh` secara periodik tiap jam. 

Untuk menjalankan file ini tiap jam dapat menambahkan syntax berikut dalam `crontab -e`
``` 
0 * * * * bash aggregate_minutes_to_hourly_log.sh
```

Data akan dituliskan pada file log dengan format nama `metrics_agg_{YmdH}.log` dan disimpan pada directory `/home/{user}/log`
```bash
Path=/home/$(whoami)
logPath=/home/$(whoami)/log
Date=$(date -d "1 hour ago" "+%Y%m%d%H")
```
`Path` dan `logPath` masih sama dengan script `minute_log.sh`. Command `date -d "1 hour ago"` akan mengambil waktu satu jam yang lalu, hal ini diperlukan karena file log yang akan diproses dibuat 1~60 menit yang lalu dan memiliki nama file (bagian jam) yang berbeda. Format date hanya sampai jam (`YmdH`), hal ini akan digunakan untuk penamaan file, serta untuk mencari file log metrics dalam jangka waktu jam yang sama.
```bash
awk -F "," '
FNR==1{next}
	# kode
' $logPath/metrics_$Date*.log > $logPath/metrics_agg_$Date.log
```
Untuk memproses data dari log yang banyak, dapat menggunakan awk. `-F ","` karena file metrics menggunakan delimiter ",". Header dari tiap file berada pada baris ke-1 dan dapat di-skip menggunakan `FNR==1 {next}`. Untuk mengambil semua file dalam jangka waktu yang sama, file input berupa `$logPath/metrics_$Date*.log`. Dimana Date disini hanya berformat `YmdH` sedangkan File aslinya berformat `YmdHMS`. `*` akan membebaskan perbedaan menit dan detik. 
```bash
awk -F "," '
FNR==1{next}
NR==2{
    for (i=1; i<=11; i++) {
        sum[i]=$i
        min[i]=$i
        max[i]=$i
    }
    count++
}
{
    for (i=1; i<=11; i++) {
        if (i==10) continue # skip path
        sum[i]+=$i
        if (min[i]>$i) min[i]=$i
        if (max[i]<$i) max[i]=$i
    }
    count++
}
' $logPath/metrics_$Date*.log > $logPath/metrics_agg_$Date.log
```
Dibuat 3 Array untuk mencatat `sum`, `min`, dan `max` dari tiap kolom.

`NR==2` digunakan untuk mengambil data yang pertama dibaca dan digunakan untuk memberi pembanding awal untuk minimum dan maximum. 

Loop digunakan untuk mengiterasi kolom. Ada 11 kolom pada file metrics. Loop dimulai dari 1 karena index akan digunakan untuk mengambil isi kolom ke-i menggunakan $i ($0 akan membaca semua kolom). `if (i==10) continue` untuk bagian Path, di-skip karena Path hasil `du -s $Path` akan sama.
```bash
awk -F "," '
END { 
    printf("type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n")
    
    printf("minimum")
    for (i=1; i<=11; i++) {
        if (i>0) printf(",")
        printf("%s",min[i])
    }
    printf("\n")

    printf("maximum")
    for (i=1; i<=11; i++) {
        if (i>0) printf(",")
        printf("%s", max[i])
    }
    printf("\n")
    
    printf("average")
    for (i=1; i<=11; i++) {
        if (i>0) printf(",")
        avg=sum[i]/count
        printf("%s", (i==10)?sum[i]:avg) # print path as is
        if (i==11) print substr(min[i],length(min[i]),1)
    }
}' $logPath/metrics_$Date*.log > $logPath/metrics_agg_$Date.log
```
Semua print menggunakan printf untuk menghindari trailing newline. `(i==10)?sum[i]:avg` digunakan untuk mengeprint path secara normal. `if (i==11) print substr(min[i],length(min[i]),1)` digunakan untuk menambahkan unit di path size.
```bash
chmod 400 $logPath/metrics_$Date.log
```
Ubah permission file menggunakan `chmod 400`.
Hasil Run:<br/>
![Agg Metrics](https://imgur.com/3WOoAdc.png)<br/>
Konfigurasi Cron<br/>
![Cron Config](https://imgur.com/XHKGtx1.png)<br/>
Hasil Cron<br/>
![Cron Result](https://imgur.com/vqwf2Xi.png)<br/>
