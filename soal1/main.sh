#!/bin/bash

log_write() {
    currTime=`date +"%D %T"`
    logAct=$1

    case "$logAct" in
    "1")
        echo "$currTime REGISTER:ERROR User Already exists" >> log.txt
        ;;
    "2")
        echo "$currTime REGISTER:INFO User $2 registered successfully" >> log.txt
        ;;
    "3")
        echo "$currTime LOGIN:ERROR Failed login attempt on user $2 " >> log.txt
        ;;
    "4")
        echo "$currTime LOGIN:INFO User $2 logged in" >> log.txt
        ;;
    *)
        echo "ERROR LOG ACTION"
        ;;
    esac
}

log_count() {
    awk -v name="$1" '($3=="LOGIN:ERROR" && $9==name) || ($3=="LOGIN:INFO" && $5==name) { count++ } END {print count}' log.txt
}

wrong() {
    echo "Username/password salah"
}

dl_image() {
    zipPass=$(awk -v id="$2 " '$0 ~ id {print $2}' users/user.txt)

    currTime=`date +"%Y-%m-%d"`
    fileDir="${currTime}_$2"

    startNum=0

    if [ -f "$fileDir.zip" ]
    then 
        unzip -P $zipPass $fileDir.zip
        rm $fileDir.zip

        cd $fileDir
        startNum=`ls PIC_* | sort -n | tail -1 | tr -dc '0-9'`
        cd ..
    else
        mkdir -p $fileDir
    fi

    for i in $( eval echo {1..$1} )
    do
        x=$(($startNum+$i))
        wget https://loremflickr.com/320/240 -O $fileDir/PIC_$(echo $x | awk '{ printf "%02d", $1}').jpeg
    done

    if [ -f "$fileDir.zip" ]
    then rm $fileDir.zip
    fi

    zip -P $zipPass -r $fileDir.zip $fileDir
    rm -r $fileDir
}

isLoggedIn=0

until [ $isLoggedIn == 1 ]
do
    read -p "Username: " username;
    read -s -p "Password: " password;


    if grep -q "$username " ./users/user.txt
    then
        if [ $password == $(awk -v id="$username " '$0 ~ id {print $2}' users/user.txt) ]
        then
            echo
            echo "Login berhasil!"
            isLoggedIn=1
            log_write "4" "$username" 
        else
            printf "\nPassword salah!\n\n"
            log_write "3" "$username" 
        fi
    else
        printf "\nUsername tidak ditemukan!\n\n"
    fi
done

echo "Command bisa dimulai"

isExit=0

until [ $isExit == 1 ]
do
    printf "\nCommand:
dl N - Mengunduh N gambar
att - Menghitung jumlah percobaan login
exit - Keluar dari program\n"

    read cmd N

    case "$cmd" in
    "dl")
        dl_image "$N" "$username"
        echo "$N gambar berhasil diunduh"
        ;;
    "att")
        echo -n "Jumlah percobaan login: "
        log_count "$username"
        ;;
    "exit")
        isExit=1
        echo "Logging out.."
        ;;
    esac
done
