#!/bin/bash

# Membuat folder
mkdir -p forensic_log_website_daffainfo_log

awk -F ":" '
NR == 1 {next} 
NR == 2 {StartHour=$3} 
{
	ReqCount++
	ipFreq[$1]++
}
$3 == "02" {ipListJam2[$1]++}
/curl/ {++curlCount}
END {
	TotalHour=$3-StartHour+1
	printf("Rata-rata serangan adalah sebanyak %f requests per jam", ReqCount/TotalHour) > "ratarata.txt"

	maxFreq=0; maxIp="0"
	for (ip in ipFreq) {
		if(maxFreq<ipFreq[ip]) {
			maxIp=ip
			maxFreq=ipFreq[ip]
		}
	}
	printf("IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n\n", maxIp, maxFreq)
	printf("Ada %d requests yang menggunakan curl sebagai user agent\n\n", curlCount)

	for (ip in ipListJam2) {
		print ip
	}
}' log_website_daffainfo.log > hasil.txt

mv hasil.txt ratarata.txt forensic_log_website_daffainfo_log
